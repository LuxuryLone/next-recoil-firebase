// Import the functions you need from the SDKs you need
import { initializeApp, getApp, getApps } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyA3fnxw-bpLtay9h0ssoLN20AUDWj8LtUg',
  authDomain: 'twitter-clone-recoil.firebaseapp.com',
  projectId: 'twitter-clone-recoil',
  storageBucket: 'twitter-clone-recoil.appspot.com',
  messagingSenderId: '385876312349',
  appId: '1:385876312349:web:39385e974bc68bc01721f4',
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const storage = getStorage();

export default app;
export { db, storage };
